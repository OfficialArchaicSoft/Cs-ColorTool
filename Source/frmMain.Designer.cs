﻿namespace ColorTool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.rdoFromTransparent = new System.Windows.Forms.RadioButton();
            this.rdoFromPixel = new System.Windows.Forms.RadioButton();
            this.boxFrom = new System.Windows.Forms.GroupBox();
            this.btnFromColorDialog = new System.Windows.Forms.Button();
            this.picFromRGB = new System.Windows.Forms.PictureBox();
            this.lblFromY = new System.Windows.Forms.Label();
            this.lblFromX = new System.Windows.Forms.Label();
            this.txtFromX = new System.Windows.Forms.TextBox();
            this.txtFromY = new System.Windows.Forms.TextBox();
            this.rdoFromKnown = new System.Windows.Forms.RadioButton();
            this.rdoFromRGB = new System.Windows.Forms.RadioButton();
            this.cmbFromKnown = new System.Windows.Forms.ComboBox();
            this.txtFromB = new System.Windows.Forms.TextBox();
            this.txtFromG = new System.Windows.Forms.TextBox();
            this.lblFromG = new System.Windows.Forms.Label();
            this.lblFromB = new System.Windows.Forms.Label();
            this.lblFromR = new System.Windows.Forms.Label();
            this.picFromKnown = new System.Windows.Forms.PictureBox();
            this.txtFromR = new System.Windows.Forms.TextBox();
            this.boxTo = new System.Windows.Forms.GroupBox();
            this.btnToColorDialog = new System.Windows.Forms.Button();
            this.picToRGB = new System.Windows.Forms.PictureBox();
            this.picToKnown = new System.Windows.Forms.PictureBox();
            this.lblToY = new System.Windows.Forms.Label();
            this.lblToX = new System.Windows.Forms.Label();
            this.txtToX = new System.Windows.Forms.TextBox();
            this.txtToY = new System.Windows.Forms.TextBox();
            this.rdoToKnown = new System.Windows.Forms.RadioButton();
            this.rdoToRGB = new System.Windows.Forms.RadioButton();
            this.cmbToKnown = new System.Windows.Forms.ComboBox();
            this.rdoToPixel = new System.Windows.Forms.RadioButton();
            this.txtToB = new System.Windows.Forms.TextBox();
            this.rdoToTransparent = new System.Windows.Forms.RadioButton();
            this.txtToG = new System.Windows.Forms.TextBox();
            this.lblToG = new System.Windows.Forms.Label();
            this.lblToB = new System.Windows.Forms.Label();
            this.lblToR = new System.Windows.Forms.Label();
            this.txtToR = new System.Windows.Forms.TextBox();
            this.boxDirectory = new System.Windows.Forms.GroupBox();
            this.boxExtensions = new System.Windows.Forms.GroupBox();
            this.chkExt = new System.Windows.Forms.CheckedListBox();
            this.btnDirectorySearch = new System.Windows.Forms.Button();
            this.txtDirectory = new System.Windows.Forms.TextBox();
            this.boxFile = new System.Windows.Forms.GroupBox();
            this.btnFileSearch = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnFileConvert = new System.Windows.Forms.Button();
            this.btnDirectoryConvert = new System.Windows.Forms.Button();
            this.boxSave = new System.Windows.Forms.GroupBox();
            this.btnSaveSearch = new System.Windows.Forms.Button();
            this.txtSave = new System.Windows.Forms.TextBox();
            this.dialogColor = new System.Windows.Forms.ColorDialog();
            this.dialogFile = new System.Windows.Forms.OpenFileDialog();
            this.dialogFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbExt = new System.Windows.Forms.ComboBox();
            this.chkConvert = new System.Windows.Forms.CheckBox();
            this.boxFrom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFromRGB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFromKnown)).BeginInit();
            this.boxTo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToRGB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToKnown)).BeginInit();
            this.boxDirectory.SuspendLayout();
            this.boxExtensions.SuspendLayout();
            this.boxFile.SuspendLayout();
            this.boxSave.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rdoFromTransparent
            // 
            this.rdoFromTransparent.AutoSize = true;
            this.rdoFromTransparent.Checked = true;
            this.rdoFromTransparent.Location = new System.Drawing.Point(13, 21);
            this.rdoFromTransparent.Name = "rdoFromTransparent";
            this.rdoFromTransparent.Size = new System.Drawing.Size(107, 21);
            this.rdoFromTransparent.TabIndex = 42;
            this.rdoFromTransparent.TabStop = true;
            this.rdoFromTransparent.Text = "Transparent";
            this.rdoFromTransparent.UseVisualStyleBackColor = true;
            // 
            // rdoFromPixel
            // 
            this.rdoFromPixel.AutoSize = true;
            this.rdoFromPixel.Location = new System.Drawing.Point(13, 48);
            this.rdoFromPixel.Name = "rdoFromPixel";
            this.rdoFromPixel.Size = new System.Drawing.Size(58, 21);
            this.rdoFromPixel.TabIndex = 43;
            this.rdoFromPixel.Text = "Pixel";
            this.rdoFromPixel.UseVisualStyleBackColor = true;
            this.rdoFromPixel.CheckedChanged += new System.EventHandler(this.rdoFromPixel_CheckedChanged);
            // 
            // boxFrom
            // 
            this.boxFrom.Controls.Add(this.btnFromColorDialog);
            this.boxFrom.Controls.Add(this.picFromRGB);
            this.boxFrom.Controls.Add(this.lblFromY);
            this.boxFrom.Controls.Add(this.lblFromX);
            this.boxFrom.Controls.Add(this.txtFromX);
            this.boxFrom.Controls.Add(this.txtFromY);
            this.boxFrom.Controls.Add(this.rdoFromKnown);
            this.boxFrom.Controls.Add(this.rdoFromRGB);
            this.boxFrom.Controls.Add(this.cmbFromKnown);
            this.boxFrom.Controls.Add(this.rdoFromPixel);
            this.boxFrom.Controls.Add(this.txtFromB);
            this.boxFrom.Controls.Add(this.rdoFromTransparent);
            this.boxFrom.Controls.Add(this.txtFromG);
            this.boxFrom.Controls.Add(this.lblFromG);
            this.boxFrom.Controls.Add(this.lblFromB);
            this.boxFrom.Controls.Add(this.lblFromR);
            this.boxFrom.Controls.Add(this.picFromKnown);
            this.boxFrom.Controls.Add(this.txtFromR);
            this.boxFrom.Location = new System.Drawing.Point(12, 12);
            this.boxFrom.Name = "boxFrom";
            this.boxFrom.Size = new System.Drawing.Size(375, 140);
            this.boxFrom.TabIndex = 44;
            this.boxFrom.TabStop = false;
            this.boxFrom.Text = "From Color";
            // 
            // btnFromColorDialog
            // 
            this.btnFromColorDialog.Enabled = false;
            this.btnFromColorDialog.Location = new System.Drawing.Point(297, 48);
            this.btnFromColorDialog.Name = "btnFromColorDialog";
            this.btnFromColorDialog.Size = new System.Drawing.Size(68, 23);
            this.btnFromColorDialog.TabIndex = 59;
            this.btnFromColorDialog.Text = "Color";
            this.btnFromColorDialog.UseVisualStyleBackColor = true;
            this.btnFromColorDialog.Click += new System.EventHandler(this.btnFromColorDialog_Click);
            // 
            // picFromRGB
            // 
            this.picFromRGB.BackColor = System.Drawing.Color.White;
            this.picFromRGB.Location = new System.Drawing.Point(297, 74);
            this.picFromRGB.Name = "picFromRGB";
            this.picFromRGB.Size = new System.Drawing.Size(68, 22);
            this.picFromRGB.TabIndex = 58;
            this.picFromRGB.TabStop = false;
            // 
            // lblFromY
            // 
            this.lblFromY.AutoSize = true;
            this.lblFromY.Enabled = false;
            this.lblFromY.Location = new System.Drawing.Point(149, 50);
            this.lblFromY.Name = "lblFromY";
            this.lblFromY.Size = new System.Drawing.Size(21, 17);
            this.lblFromY.TabIndex = 57;
            this.lblFromY.Text = "Y:";
            // 
            // lblFromX
            // 
            this.lblFromX.AutoSize = true;
            this.lblFromX.Enabled = false;
            this.lblFromX.Location = new System.Drawing.Point(78, 50);
            this.lblFromX.Name = "lblFromX";
            this.lblFromX.Size = new System.Drawing.Size(21, 17);
            this.lblFromX.TabIndex = 56;
            this.lblFromX.Text = "X:";
            // 
            // txtFromX
            // 
            this.txtFromX.Enabled = false;
            this.txtFromX.Location = new System.Drawing.Point(102, 47);
            this.txtFromX.Name = "txtFromX";
            this.txtFromX.Size = new System.Drawing.Size(39, 22);
            this.txtFromX.TabIndex = 55;
            this.txtFromX.Text = "0";
            // 
            // txtFromY
            // 
            this.txtFromY.Enabled = false;
            this.txtFromY.Location = new System.Drawing.Point(172, 47);
            this.txtFromY.Name = "txtFromY";
            this.txtFromY.Size = new System.Drawing.Size(39, 22);
            this.txtFromY.TabIndex = 54;
            this.txtFromY.Text = "0";
            // 
            // rdoFromKnown
            // 
            this.rdoFromKnown.AutoSize = true;
            this.rdoFromKnown.Location = new System.Drawing.Point(13, 102);
            this.rdoFromKnown.Name = "rdoFromKnown";
            this.rdoFromKnown.Size = new System.Drawing.Size(108, 21);
            this.rdoFromKnown.TabIndex = 52;
            this.rdoFromKnown.Text = "Known Color";
            this.rdoFromKnown.UseVisualStyleBackColor = true;
            this.rdoFromKnown.CheckedChanged += new System.EventHandler(this.rdoFromKnown_CheckedChanged);
            // 
            // rdoFromRGB
            // 
            this.rdoFromRGB.AutoSize = true;
            this.rdoFromRGB.Location = new System.Drawing.Point(13, 75);
            this.rdoFromRGB.Name = "rdoFromRGB";
            this.rdoFromRGB.Size = new System.Drawing.Size(59, 21);
            this.rdoFromRGB.TabIndex = 51;
            this.rdoFromRGB.Text = "RGB";
            this.rdoFromRGB.UseVisualStyleBackColor = true;
            this.rdoFromRGB.CheckedChanged += new System.EventHandler(this.rdoFromRGB_CheckedChanged);
            // 
            // cmbFromKnown
            // 
            this.cmbFromKnown.BackColor = System.Drawing.Color.White;
            this.cmbFromKnown.Enabled = false;
            this.cmbFromKnown.FormattingEnabled = true;
            this.cmbFromKnown.Location = new System.Drawing.Point(135, 101);
            this.cmbFromKnown.Name = "cmbFromKnown";
            this.cmbFromKnown.Size = new System.Drawing.Size(145, 24);
            this.cmbFromKnown.TabIndex = 49;
            this.cmbFromKnown.Text = "ActiveBorder";
            this.cmbFromKnown.SelectedIndexChanged += new System.EventHandler(this.cmbFromKnown_SelectedIndexChanged);
            this.cmbFromKnown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbFromKnown_KeyDown);
            // 
            // txtFromB
            // 
            this.txtFromB.BackColor = System.Drawing.Color.Blue;
            this.txtFromB.Enabled = false;
            this.txtFromB.Location = new System.Drawing.Point(241, 74);
            this.txtFromB.Name = "txtFromB";
            this.txtFromB.Size = new System.Drawing.Size(39, 22);
            this.txtFromB.TabIndex = 48;
            this.txtFromB.Text = "255";
            this.txtFromB.TextChanged += new System.EventHandler(this.txtFromB_TextChanged);
            // 
            // txtFromG
            // 
            this.txtFromG.BackColor = System.Drawing.Color.Lime;
            this.txtFromG.Enabled = false;
            this.txtFromG.Location = new System.Drawing.Point(172, 74);
            this.txtFromG.Name = "txtFromG";
            this.txtFromG.Size = new System.Drawing.Size(39, 22);
            this.txtFromG.TabIndex = 47;
            this.txtFromG.Text = "255";
            this.txtFromG.TextChanged += new System.EventHandler(this.txtFromG_TextChanged);
            // 
            // lblFromG
            // 
            this.lblFromG.AutoSize = true;
            this.lblFromG.Enabled = false;
            this.lblFromG.Location = new System.Drawing.Point(147, 77);
            this.lblFromG.Name = "lblFromG";
            this.lblFromG.Size = new System.Drawing.Size(23, 17);
            this.lblFromG.TabIndex = 46;
            this.lblFromG.Text = "G:";
            // 
            // lblFromB
            // 
            this.lblFromB.AutoSize = true;
            this.lblFromB.Enabled = false;
            this.lblFromB.Location = new System.Drawing.Point(217, 77);
            this.lblFromB.Name = "lblFromB";
            this.lblFromB.Size = new System.Drawing.Size(21, 17);
            this.lblFromB.TabIndex = 45;
            this.lblFromB.Text = "B:";
            // 
            // lblFromR
            // 
            this.lblFromR.AutoSize = true;
            this.lblFromR.Enabled = false;
            this.lblFromR.Location = new System.Drawing.Point(78, 77);
            this.lblFromR.Name = "lblFromR";
            this.lblFromR.Size = new System.Drawing.Size(22, 17);
            this.lblFromR.TabIndex = 44;
            this.lblFromR.Text = "R:";
            // 
            // picFromKnown
            // 
            this.picFromKnown.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.picFromKnown.Location = new System.Drawing.Point(297, 101);
            this.picFromKnown.Name = "picFromKnown";
            this.picFromKnown.Size = new System.Drawing.Size(68, 24);
            this.picFromKnown.TabIndex = 43;
            this.picFromKnown.TabStop = false;
            // 
            // txtFromR
            // 
            this.txtFromR.BackColor = System.Drawing.Color.Red;
            this.txtFromR.Enabled = false;
            this.txtFromR.Location = new System.Drawing.Point(102, 74);
            this.txtFromR.Name = "txtFromR";
            this.txtFromR.Size = new System.Drawing.Size(39, 22);
            this.txtFromR.TabIndex = 42;
            this.txtFromR.Text = "255";
            this.txtFromR.TextChanged += new System.EventHandler(this.txtFromR_TextChanged);
            // 
            // boxTo
            // 
            this.boxTo.Controls.Add(this.btnToColorDialog);
            this.boxTo.Controls.Add(this.picToRGB);
            this.boxTo.Controls.Add(this.picToKnown);
            this.boxTo.Controls.Add(this.lblToY);
            this.boxTo.Controls.Add(this.lblToX);
            this.boxTo.Controls.Add(this.txtToX);
            this.boxTo.Controls.Add(this.txtToY);
            this.boxTo.Controls.Add(this.rdoToKnown);
            this.boxTo.Controls.Add(this.rdoToRGB);
            this.boxTo.Controls.Add(this.cmbToKnown);
            this.boxTo.Controls.Add(this.rdoToPixel);
            this.boxTo.Controls.Add(this.txtToB);
            this.boxTo.Controls.Add(this.rdoToTransparent);
            this.boxTo.Controls.Add(this.txtToG);
            this.boxTo.Controls.Add(this.lblToG);
            this.boxTo.Controls.Add(this.lblToB);
            this.boxTo.Controls.Add(this.lblToR);
            this.boxTo.Controls.Add(this.txtToR);
            this.boxTo.Location = new System.Drawing.Point(12, 158);
            this.boxTo.Name = "boxTo";
            this.boxTo.Size = new System.Drawing.Size(375, 140);
            this.boxTo.TabIndex = 45;
            this.boxTo.TabStop = false;
            this.boxTo.Text = "To Color";
            // 
            // btnToColorDialog
            // 
            this.btnToColorDialog.Enabled = false;
            this.btnToColorDialog.Location = new System.Drawing.Point(296, 48);
            this.btnToColorDialog.Name = "btnToColorDialog";
            this.btnToColorDialog.Size = new System.Drawing.Size(69, 23);
            this.btnToColorDialog.TabIndex = 60;
            this.btnToColorDialog.Text = "Color";
            this.btnToColorDialog.UseVisualStyleBackColor = true;
            this.btnToColorDialog.Click += new System.EventHandler(this.btnToColorDialog_Click);
            // 
            // picToRGB
            // 
            this.picToRGB.BackColor = System.Drawing.Color.White;
            this.picToRGB.Location = new System.Drawing.Point(297, 74);
            this.picToRGB.Name = "picToRGB";
            this.picToRGB.Size = new System.Drawing.Size(68, 22);
            this.picToRGB.TabIndex = 59;
            this.picToRGB.TabStop = false;
            // 
            // picToKnown
            // 
            this.picToKnown.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.picToKnown.Location = new System.Drawing.Point(297, 101);
            this.picToKnown.Name = "picToKnown";
            this.picToKnown.Size = new System.Drawing.Size(68, 24);
            this.picToKnown.TabIndex = 58;
            this.picToKnown.TabStop = false;
            // 
            // lblToY
            // 
            this.lblToY.AutoSize = true;
            this.lblToY.Enabled = false;
            this.lblToY.Location = new System.Drawing.Point(149, 50);
            this.lblToY.Name = "lblToY";
            this.lblToY.Size = new System.Drawing.Size(21, 17);
            this.lblToY.TabIndex = 57;
            this.lblToY.Text = "Y:";
            // 
            // lblToX
            // 
            this.lblToX.AutoSize = true;
            this.lblToX.Enabled = false;
            this.lblToX.Location = new System.Drawing.Point(78, 50);
            this.lblToX.Name = "lblToX";
            this.lblToX.Size = new System.Drawing.Size(21, 17);
            this.lblToX.TabIndex = 56;
            this.lblToX.Text = "X:";
            // 
            // txtToX
            // 
            this.txtToX.Enabled = false;
            this.txtToX.Location = new System.Drawing.Point(102, 47);
            this.txtToX.Name = "txtToX";
            this.txtToX.Size = new System.Drawing.Size(39, 22);
            this.txtToX.TabIndex = 55;
            this.txtToX.Text = "0";
            // 
            // txtToY
            // 
            this.txtToY.Enabled = false;
            this.txtToY.Location = new System.Drawing.Point(172, 47);
            this.txtToY.Name = "txtToY";
            this.txtToY.Size = new System.Drawing.Size(39, 22);
            this.txtToY.TabIndex = 54;
            this.txtToY.Text = "0";
            // 
            // rdoToKnown
            // 
            this.rdoToKnown.AutoSize = true;
            this.rdoToKnown.Location = new System.Drawing.Point(13, 102);
            this.rdoToKnown.Name = "rdoToKnown";
            this.rdoToKnown.Size = new System.Drawing.Size(108, 21);
            this.rdoToKnown.TabIndex = 52;
            this.rdoToKnown.Text = "Known Color";
            this.rdoToKnown.UseVisualStyleBackColor = true;
            this.rdoToKnown.CheckedChanged += new System.EventHandler(this.rdoToKnown_CheckedChanged);
            // 
            // rdoToRGB
            // 
            this.rdoToRGB.AutoSize = true;
            this.rdoToRGB.Location = new System.Drawing.Point(13, 75);
            this.rdoToRGB.Name = "rdoToRGB";
            this.rdoToRGB.Size = new System.Drawing.Size(59, 21);
            this.rdoToRGB.TabIndex = 51;
            this.rdoToRGB.Text = "RGB";
            this.rdoToRGB.UseVisualStyleBackColor = true;
            this.rdoToRGB.CheckedChanged += new System.EventHandler(this.rdoToRGB_CheckedChanged);
            // 
            // cmbToKnown
            // 
            this.cmbToKnown.BackColor = System.Drawing.Color.White;
            this.cmbToKnown.Enabled = false;
            this.cmbToKnown.FormattingEnabled = true;
            this.cmbToKnown.Location = new System.Drawing.Point(135, 101);
            this.cmbToKnown.Name = "cmbToKnown";
            this.cmbToKnown.Size = new System.Drawing.Size(145, 24);
            this.cmbToKnown.TabIndex = 49;
            this.cmbToKnown.Text = "ActiveBorder";
            this.cmbToKnown.SelectedIndexChanged += new System.EventHandler(this.cmbToKnown_SelectedIndexChanged);
            this.cmbToKnown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbToKnown_KeyDown);
            // 
            // rdoToPixel
            // 
            this.rdoToPixel.AutoSize = true;
            this.rdoToPixel.Location = new System.Drawing.Point(13, 48);
            this.rdoToPixel.Name = "rdoToPixel";
            this.rdoToPixel.Size = new System.Drawing.Size(58, 21);
            this.rdoToPixel.TabIndex = 43;
            this.rdoToPixel.Text = "Pixel";
            this.rdoToPixel.UseVisualStyleBackColor = true;
            this.rdoToPixel.CheckedChanged += new System.EventHandler(this.rdoToPixel_CheckedChanged);
            // 
            // txtToB
            // 
            this.txtToB.BackColor = System.Drawing.Color.Blue;
            this.txtToB.Enabled = false;
            this.txtToB.Location = new System.Drawing.Point(241, 74);
            this.txtToB.Name = "txtToB";
            this.txtToB.Size = new System.Drawing.Size(39, 22);
            this.txtToB.TabIndex = 48;
            this.txtToB.Text = "255";
            this.txtToB.TextChanged += new System.EventHandler(this.txtToB_TextChanged);
            // 
            // rdoToTransparent
            // 
            this.rdoToTransparent.AutoSize = true;
            this.rdoToTransparent.Checked = true;
            this.rdoToTransparent.Location = new System.Drawing.Point(13, 21);
            this.rdoToTransparent.Name = "rdoToTransparent";
            this.rdoToTransparent.Size = new System.Drawing.Size(107, 21);
            this.rdoToTransparent.TabIndex = 42;
            this.rdoToTransparent.TabStop = true;
            this.rdoToTransparent.Text = "Transparent";
            this.rdoToTransparent.UseVisualStyleBackColor = true;
            // 
            // txtToG
            // 
            this.txtToG.BackColor = System.Drawing.Color.Lime;
            this.txtToG.Enabled = false;
            this.txtToG.Location = new System.Drawing.Point(172, 74);
            this.txtToG.Name = "txtToG";
            this.txtToG.Size = new System.Drawing.Size(39, 22);
            this.txtToG.TabIndex = 47;
            this.txtToG.Text = "255";
            this.txtToG.TextChanged += new System.EventHandler(this.txtToG_TextChanged);
            // 
            // lblToG
            // 
            this.lblToG.AutoSize = true;
            this.lblToG.Enabled = false;
            this.lblToG.Location = new System.Drawing.Point(147, 77);
            this.lblToG.Name = "lblToG";
            this.lblToG.Size = new System.Drawing.Size(23, 17);
            this.lblToG.TabIndex = 46;
            this.lblToG.Text = "G:";
            // 
            // lblToB
            // 
            this.lblToB.AutoSize = true;
            this.lblToB.Enabled = false;
            this.lblToB.Location = new System.Drawing.Point(217, 77);
            this.lblToB.Name = "lblToB";
            this.lblToB.Size = new System.Drawing.Size(21, 17);
            this.lblToB.TabIndex = 45;
            this.lblToB.Text = "B:";
            // 
            // lblToR
            // 
            this.lblToR.AutoSize = true;
            this.lblToR.Enabled = false;
            this.lblToR.Location = new System.Drawing.Point(78, 77);
            this.lblToR.Name = "lblToR";
            this.lblToR.Size = new System.Drawing.Size(22, 17);
            this.lblToR.TabIndex = 44;
            this.lblToR.Text = "R:";
            // 
            // txtToR
            // 
            this.txtToR.BackColor = System.Drawing.Color.Red;
            this.txtToR.Enabled = false;
            this.txtToR.Location = new System.Drawing.Point(102, 74);
            this.txtToR.Name = "txtToR";
            this.txtToR.Size = new System.Drawing.Size(39, 22);
            this.txtToR.TabIndex = 42;
            this.txtToR.Text = "255";
            this.txtToR.TextChanged += new System.EventHandler(this.txtToR_TextChanged);
            // 
            // boxDirectory
            // 
            this.boxDirectory.Controls.Add(this.groupBox1);
            this.boxDirectory.Controls.Add(this.boxExtensions);
            this.boxDirectory.Controls.Add(this.btnDirectorySearch);
            this.boxDirectory.Controls.Add(this.txtDirectory);
            this.boxDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxDirectory.Location = new System.Drawing.Point(12, 362);
            this.boxDirectory.Name = "boxDirectory";
            this.boxDirectory.Size = new System.Drawing.Size(375, 154);
            this.boxDirectory.TabIndex = 0;
            this.boxDirectory.TabStop = false;
            this.boxDirectory.Text = "By Directory";
            // 
            // boxExtensions
            // 
            this.boxExtensions.Controls.Add(this.chkExt);
            this.boxExtensions.Location = new System.Drawing.Point(13, 49);
            this.boxExtensions.Name = "boxExtensions";
            this.boxExtensions.Size = new System.Drawing.Size(98, 100);
            this.boxExtensions.TabIndex = 59;
            this.boxExtensions.TabStop = false;
            this.boxExtensions.Text = "Extensions";
            // 
            // chkExt
            // 
            this.chkExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExt.FormattingEnabled = true;
            this.chkExt.Items.AddRange(new object[] {
            ".jpg",
            ".bmp",
            ".png"});
            this.chkExt.Location = new System.Drawing.Point(6, 21);
            this.chkExt.Name = "chkExt";
            this.chkExt.Size = new System.Drawing.Size(85, 70);
            this.chkExt.TabIndex = 54;
            // 
            // btnDirectorySearch
            // 
            this.btnDirectorySearch.Location = new System.Drawing.Point(13, 21);
            this.btnDirectorySearch.Name = "btnDirectorySearch";
            this.btnDirectorySearch.Size = new System.Drawing.Size(86, 25);
            this.btnDirectorySearch.TabIndex = 56;
            this.btnDirectorySearch.Text = "Search";
            this.btnDirectorySearch.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDirectorySearch.UseVisualStyleBackColor = true;
            this.btnDirectorySearch.Click += new System.EventHandler(this.btnDirectorySearch_Click);
            // 
            // txtDirectory
            // 
            this.txtDirectory.Location = new System.Drawing.Point(105, 21);
            this.txtDirectory.Name = "txtDirectory";
            this.txtDirectory.Size = new System.Drawing.Size(260, 22);
            this.txtDirectory.TabIndex = 54;
            this.txtDirectory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDirectory_KeyDown);
            // 
            // boxFile
            // 
            this.boxFile.Controls.Add(this.btnFileSearch);
            this.boxFile.Controls.Add(this.txtFile);
            this.boxFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxFile.Location = new System.Drawing.Point(12, 304);
            this.boxFile.Name = "boxFile";
            this.boxFile.Size = new System.Drawing.Size(375, 52);
            this.boxFile.TabIndex = 46;
            this.boxFile.TabStop = false;
            this.boxFile.Text = "By File";
            // 
            // btnFileSearch
            // 
            this.btnFileSearch.Location = new System.Drawing.Point(13, 21);
            this.btnFileSearch.Name = "btnFileSearch";
            this.btnFileSearch.Size = new System.Drawing.Size(86, 25);
            this.btnFileSearch.TabIndex = 56;
            this.btnFileSearch.Text = "Search";
            this.btnFileSearch.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFileSearch.UseVisualStyleBackColor = true;
            this.btnFileSearch.Click += new System.EventHandler(this.btnFileSearch_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(105, 21);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(260, 22);
            this.txtFile.TabIndex = 54;
            this.txtFile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFile_KeyDown);
            // 
            // btnFileConvert
            // 
            this.btnFileConvert.Enabled = false;
            this.btnFileConvert.Location = new System.Drawing.Point(105, 594);
            this.btnFileConvert.Name = "btnFileConvert";
            this.btnFileConvert.Size = new System.Drawing.Size(138, 27);
            this.btnFileConvert.TabIndex = 57;
            this.btnFileConvert.Text = "Convert File";
            this.btnFileConvert.UseVisualStyleBackColor = true;
            this.btnFileConvert.Click += new System.EventHandler(this.btnFileConvert_Click);
            // 
            // btnDirectoryConvert
            // 
            this.btnDirectoryConvert.Enabled = false;
            this.btnDirectoryConvert.Location = new System.Drawing.Point(249, 594);
            this.btnDirectoryConvert.Name = "btnDirectoryConvert";
            this.btnDirectoryConvert.Size = new System.Drawing.Size(138, 27);
            this.btnDirectoryConvert.TabIndex = 61;
            this.btnDirectoryConvert.Text = "Convert Directory";
            this.btnDirectoryConvert.UseVisualStyleBackColor = true;
            this.btnDirectoryConvert.Click += new System.EventHandler(this.btnDirectoryConvert_Click);
            // 
            // boxSave
            // 
            this.boxSave.Controls.Add(this.btnSaveSearch);
            this.boxSave.Controls.Add(this.txtSave);
            this.boxSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxSave.Location = new System.Drawing.Point(12, 522);
            this.boxSave.Name = "boxSave";
            this.boxSave.Size = new System.Drawing.Size(375, 67);
            this.boxSave.TabIndex = 62;
            this.boxSave.TabStop = false;
            this.boxSave.Text = "Save Location";
            // 
            // btnSaveSearch
            // 
            this.btnSaveSearch.Location = new System.Drawing.Point(16, 26);
            this.btnSaveSearch.Name = "btnSaveSearch";
            this.btnSaveSearch.Size = new System.Drawing.Size(86, 27);
            this.btnSaveSearch.TabIndex = 56;
            this.btnSaveSearch.Text = "Search";
            this.btnSaveSearch.UseVisualStyleBackColor = true;
            this.btnSaveSearch.Click += new System.EventHandler(this.btnSaveSearch_Click);
            // 
            // txtSave
            // 
            this.txtSave.Location = new System.Drawing.Point(108, 26);
            this.txtSave.Name = "txtSave";
            this.txtSave.Size = new System.Drawing.Size(257, 22);
            this.txtSave.TabIndex = 54;
            this.txtSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSave_KeyDown);
            // 
            // dialogFile
            // 
            this.dialogFile.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbExt);
            this.groupBox1.Controls.Add(this.chkConvert);
            this.groupBox1.Location = new System.Drawing.Point(124, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 105);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Extension Modifier";
            // 
            // cmbExt
            // 
            this.cmbExt.Enabled = false;
            this.cmbExt.FormattingEnabled = true;
            this.cmbExt.Items.AddRange(new object[] {
            ".jpg",
            ".bmp",
            ".png"});
            this.cmbExt.Location = new System.Drawing.Point(11, 62);
            this.cmbExt.Name = "cmbExt";
            this.cmbExt.Size = new System.Drawing.Size(230, 24);
            this.cmbExt.TabIndex = 62;
            this.cmbExt.Text = ".png";
            // 
            // chkConvert
            // 
            this.chkConvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConvert.Location = new System.Drawing.Point(11, 30);
            this.chkConvert.Name = "chkConvert";
            this.chkConvert.Size = new System.Drawing.Size(230, 26);
            this.chkConvert.TabIndex = 61;
            this.chkConvert.Text = "Convert To Extension?";
            this.chkConvert.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 629);
            this.Controls.Add(this.boxSave);
            this.Controls.Add(this.btnFileConvert);
            this.Controls.Add(this.btnDirectoryConvert);
            this.Controls.Add(this.boxFile);
            this.Controls.Add(this.boxDirectory);
            this.Controls.Add(this.boxTo);
            this.Controls.Add(this.boxFrom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Color Manipulation Tool";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.boxFrom.ResumeLayout(false);
            this.boxFrom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFromRGB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFromKnown)).EndInit();
            this.boxTo.ResumeLayout(false);
            this.boxTo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToRGB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToKnown)).EndInit();
            this.boxDirectory.ResumeLayout(false);
            this.boxDirectory.PerformLayout();
            this.boxExtensions.ResumeLayout(false);
            this.boxFile.ResumeLayout(false);
            this.boxFile.PerformLayout();
            this.boxSave.ResumeLayout(false);
            this.boxSave.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoFromTransparent;
        private System.Windows.Forms.RadioButton rdoFromPixel;
        private System.Windows.Forms.GroupBox boxFrom;
        private System.Windows.Forms.Label lblFromY;
        private System.Windows.Forms.Label lblFromX;
        private System.Windows.Forms.TextBox txtFromX;
        private System.Windows.Forms.TextBox txtFromY;
        private System.Windows.Forms.RadioButton rdoFromKnown;
        private System.Windows.Forms.RadioButton rdoFromRGB;
        private System.Windows.Forms.ComboBox cmbFromKnown;
        private System.Windows.Forms.TextBox txtFromB;
        private System.Windows.Forms.TextBox txtFromG;
        private System.Windows.Forms.Label lblFromG;
        private System.Windows.Forms.Label lblFromB;
        private System.Windows.Forms.Label lblFromR;
        private System.Windows.Forms.PictureBox picFromKnown;
        private System.Windows.Forms.TextBox txtFromR;
        private System.Windows.Forms.GroupBox boxTo;
        private System.Windows.Forms.Label lblToY;
        private System.Windows.Forms.Label lblToX;
        private System.Windows.Forms.TextBox txtToX;
        private System.Windows.Forms.TextBox txtToY;
        private System.Windows.Forms.RadioButton rdoToKnown;
        private System.Windows.Forms.RadioButton rdoToRGB;
        private System.Windows.Forms.ComboBox cmbToKnown;
        private System.Windows.Forms.RadioButton rdoToPixel;
        private System.Windows.Forms.TextBox txtToB;
        private System.Windows.Forms.RadioButton rdoToTransparent;
        private System.Windows.Forms.TextBox txtToG;
        private System.Windows.Forms.Label lblToG;
        private System.Windows.Forms.Label lblToB;
        private System.Windows.Forms.Label lblToR;
        private System.Windows.Forms.TextBox txtToR;
        private System.Windows.Forms.GroupBox boxDirectory;
        private System.Windows.Forms.GroupBox boxExtensions;
        private System.Windows.Forms.CheckedListBox chkExt;
        private System.Windows.Forms.Button btnDirectorySearch;
        private System.Windows.Forms.TextBox txtDirectory;
        private System.Windows.Forms.GroupBox boxFile;
        private System.Windows.Forms.Button btnFileSearch;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button btnFileConvert;
        private System.Windows.Forms.Button btnDirectoryConvert;
        private System.Windows.Forms.GroupBox boxSave;
        private System.Windows.Forms.Button btnSaveSearch;
        private System.Windows.Forms.TextBox txtSave;
        private System.Windows.Forms.PictureBox picFromRGB;
        private System.Windows.Forms.PictureBox picToRGB;
        private System.Windows.Forms.PictureBox picToKnown;
        private System.Windows.Forms.ColorDialog dialogColor;
        private System.Windows.Forms.OpenFileDialog dialogFile;
        private System.Windows.Forms.FolderBrowserDialog dialogFolder;
        private System.Windows.Forms.Button btnFromColorDialog;
        private System.Windows.Forms.Button btnToColorDialog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbExt;
        private System.Windows.Forms.CheckBox chkConvert;
    }
}