﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ColorTool
{
    public partial class frmMain : Form
    {
        bool file, directory, save;
        List<Control> reEnable = new List<Control>();
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            string[] names = Enum.GetNames(typeof(KnownColor));

            foreach (var s in names)
            {
                cmbFromKnown.Items.Add(s);
                cmbToKnown.Items.Add(s);
            }
        }

        #region Radio Button From
        private void rdoFromPixel_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoFromPixel.Checked)
            {
                lblFromX.Enabled = true;
                txtFromX.Enabled = true;
                lblFromY.Enabled = true;
                txtFromY.Enabled = true;
            }
            else
            {
                lblFromX.Enabled = false;
                txtFromX.Enabled = false;
                lblFromY.Enabled = false;
                txtFromY.Enabled = false;
            }
        }

        private void rdoFromRGB_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoFromRGB.Checked)
            {
                lblFromR.Enabled = true;
                txtFromR.Enabled = true;
                lblFromG.Enabled = true;
                txtFromG.Enabled = true;
                lblFromB.Enabled = true;
                txtFromB.Enabled = true;
                picFromRGB.Enabled = true;
                btnFromColorDialog.Enabled = true;
            }
            else
            {
                lblFromR.Enabled = false;
                txtFromR.Enabled = false;
                lblFromG.Enabled = false;
                txtFromG.Enabled = false;
                lblFromB.Enabled = false;
                txtFromB.Enabled = false;
                picFromRGB.Enabled = false;
                btnFromColorDialog.Enabled = false;
            }
        }

        private void rdoFromKnown_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoFromKnown.Checked)
            {
                cmbFromKnown.Enabled = true;
                picFromKnown.Enabled = true;
            }
            else
            {
                cmbFromKnown.Enabled = false;
                picFromKnown.Enabled = false;
            }
        }
        #endregion
        #region Radio Button To
        private void rdoToPixel_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoToPixel.Checked)
            {
                lblToX.Enabled = true;
                txtToX.Enabled = true;
                lblToY.Enabled = true;
                txtToY.Enabled = true;
            }
            else
            {
                lblToX.Enabled = false;
                txtToX.Enabled = false;
                lblToY.Enabled = false;
                txtToY.Enabled = false;
            }
        }

        private void rdoToRGB_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoToRGB.Checked)
            {
                lblToR.Enabled = true;
                txtToR.Enabled = true;
                lblToG.Enabled = true;
                txtToG.Enabled = true;
                lblToB.Enabled = true;
                txtToB.Enabled = true;
                picToRGB.Enabled = true;
                btnToColorDialog.Enabled = true;
            }
            else
            {
                lblToR.Enabled = false;
                txtToR.Enabled = false;
                lblToG.Enabled = false;
                txtToG.Enabled = false;
                lblToB.Enabled = false;
                txtToB.Enabled = false;
                picToRGB.Enabled = false;
                btnToColorDialog.Enabled = false;
            }
        }

        private void rdoToKnown_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoToKnown.Checked)
            {
                cmbToKnown.Enabled = true;
                picToKnown.Enabled = true;
            }
            else
            {
                cmbToKnown.Enabled = false;
                picToKnown.Enabled = false;
            }
        }
        #endregion

        #region Color From Changed
        private void SetFromPicRGB()
        {
            int r, g, b;
            int.TryParse(txtFromR.Text, out r);
            int.TryParse(txtFromG.Text, out g);
            int.TryParse(txtFromB.Text, out b);
            picFromRGB.BackColor = Color.FromArgb(255, r, g, b);
        }

        private void txtFromR_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(txtFromR.Text, out val);
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            txtFromR.Text = val.ToString();
            txtFromR.BackColor = Color.FromArgb(255, val, 0, 0);
            SetFromPicRGB();
        }

        private void txtFromG_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(txtFromG.Text, out val);
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            txtFromG.Text = val.ToString();
            txtFromG.BackColor = Color.FromArgb(255, 0, val, 0);
            SetFromPicRGB();
        }

        private void txtFromB_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(txtFromB.Text, out val);
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            txtFromB.Text = val.ToString();
            txtFromB.BackColor = Color.FromArgb(255, 0, 0, val);
            SetFromPicRGB();
        }

        private void btnFromColorDialog_Click(object sender, EventArgs e)
        {
            DialogResult result = dialogColor.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFromR.Text = dialogColor.Color.R.ToString();
                txtFromG.Text = dialogColor.Color.G.ToString();
                txtFromB.Text = dialogColor.Color.B.ToString();
            }
        }

        private void cmbFromKnown_SelectedIndexChanged(object sender, EventArgs e)
        {
            Color from;
            try { from = Color.FromName(cmbFromKnown.Text); }
            catch { return; }
            picFromKnown.BackColor = from;
        }

        private void cmbFromKnown_KeyDown(object sender, KeyEventArgs e)
        {
            foreach (string item in cmbFromKnown.Items)
                if (cmbFromKnown.Text == item)
                {
                    picFromKnown.BackColor = Color.FromName(cmbFromKnown.Text);
                    return;
                }
        }
        #endregion
        #region Color To Changed
        private void SetToPicRGB()
        {
            int r, g, b;
            int.TryParse(txtToR.Text, out r);
            int.TryParse(txtToG.Text, out g);
            int.TryParse(txtToB.Text, out b);
            picToRGB.BackColor = Color.FromArgb(255, r, g, b);
        }

        private void txtToR_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(txtToR.Text, out val);
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            txtToR.Text = val.ToString();
            txtToR.BackColor = Color.FromArgb(255, val, 0, 0);
            SetToPicRGB();
        }

        private void txtToG_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(txtToG.Text, out val);
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            txtToG.Text = val.ToString();
            txtToG.BackColor = Color.FromArgb(255, 0, val, 0);
            SetToPicRGB();
        }

        private void txtToB_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(txtToB.Text, out val);
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            txtToB.Text = val.ToString();
            txtToB.BackColor = Color.FromArgb(255, 0, 0, val);
            SetToPicRGB();
        }

        private void btnToColorDialog_Click(object sender, EventArgs e)
        {
            DialogResult result = dialogColor.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtToR.Text = dialogColor.Color.R.ToString();
                txtToG.Text = dialogColor.Color.G.ToString();
                txtToB.Text = dialogColor.Color.B.ToString();
            }
        }

        private void cmbToKnown_SelectedIndexChanged(object sender, EventArgs e)
        {
            Color to;
            try { to = Color.FromName(cmbToKnown.Text); }
            catch { return; }
            picToKnown.BackColor = to;
        }

        private void cmbToKnown_KeyDown(object sender, KeyEventArgs e)
        {
            foreach (string item in cmbToKnown.Items)
                if (cmbToKnown.Text == item)
                {
                    picToKnown.BackColor = Color.FromName(cmbToKnown.Text);
                    return;
                }
        }
        #endregion

        #region Key Supression
        private void txtFile_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

            if (e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.Left ||
                e.KeyCode == Keys.Right)
                e.SuppressKeyPress = false;
        }

        private void txtDirectory_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

            if (e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.Left ||
                e.KeyCode == Keys.Right)
                e.SuppressKeyPress = false;
        }

        private void cmbExt_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

            if (e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.Left ||
                e.KeyCode == Keys.Right)
                e.SuppressKeyPress = false;
        }

        private void txtSave_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

            if (e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.Left ||
                e.KeyCode == Keys.Right)
                e.SuppressKeyPress = false;
        }
        #endregion

        #region Search Boxes
        private void btnFileSearch_Click(object sender, EventArgs e)
        {
            dialogFile.Multiselect = false;
            DialogResult result = dialogFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtFile.Text = dialogFile.FileName;
                file = true;
            }
            if (file && save)
                btnFileConvert.Enabled = true;
        }

        private void btnDirectorySearch_Click(object sender, EventArgs e)
        {
            DialogResult result = dialogFolder.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtDirectory.Text = dialogFolder.SelectedPath;
                directory = true;
            }

            if (directory && save)
                btnDirectoryConvert.Enabled = true;
        }

        private void btnSaveSearch_Click(object sender, EventArgs e)
        {
            DialogResult result = dialogFolder.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtSave.Text = dialogFolder.SelectedPath;
                save = true;
            }

            if (file && save)
                btnFileConvert.Enabled = true;
            if (directory && save)
                btnDirectoryConvert.Enabled = true;
        }
        #endregion

        private void chkConvert_CheckedChanged(object sender, EventArgs e)
        {
            if (chkConvert.Checked)
                cmbExt.Enabled = true;
            else
                cmbExt.Enabled = false;
        }

        #region Convert Tools
        private void DeactivateControls()
        {
            foreach (Control c in this.Controls)
                if (c.Enabled)
                {
                    reEnable.Add(c);
                    c.Enabled = false;
                }

            Application.DoEvents();
        }

        private void ReactivateControls()
        {
            foreach (Control c in reEnable)
                c.Enabled = true;

            reEnable.Clear();
        }

        private string ModifyImage(ref Bitmap img, FileInfo fi)
        {
            try
            {
                #region To Transparent
                if (rdoToTransparent.Checked)
                {
                    if (rdoFromTransparent.Checked) return "";
                    else if (rdoFromPixel.Checked)
                    {
                        int x, y;
                        int.TryParse(txtFromX.Text, out x);
                        int.TryParse(txtFromY.Text, out y);
                        img.MakeTransparent(img.GetPixel(x, y));
                    }
                    else if (rdoFromRGB.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtFromR.Text, out r);
                        int.TryParse(txtFromG.Text, out g);
                        int.TryParse(txtFromB.Text, out b);
                        img.MakeTransparent(Color.FromArgb(255, r, g, b));
                    }
                    else if (rdoFromKnown.Checked)
                    {
                        Color from;
                        try { from = Color.FromName(cmbFromKnown.Text); }
                        catch { return "Error"; }
                        img.MakeTransparent(from);
                    }
                }
                #endregion
                #region To Pixel
                else if (rdoToPixel.Checked)
                {
                    if (rdoFromTransparent.Checked)
                    {
                        int x, y;
                        int.TryParse(txtToX.Text, out x);
                        int.TryParse(txtToY.Text, out y);
                        Color to = img.GetPixel(x, y);

                        for (x = 0; x < img.Width; x++)
                            for (y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y).A == 0)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromPixel.Checked)
                    {
                        int x, y;
                        int.TryParse(txtFromX.Text, out x);
                        int.TryParse(txtFromY.Text, out y);
                        Color from = img.GetPixel(x, y);
                        int.TryParse(txtToX.Text, out x);
                        int.TryParse(txtToY.Text, out y);
                        Color to = img.GetPixel(x, y);

                        for (x = 0; x < img.Width; x++)
                            for (y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == from)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromRGB.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtFromR.Text, out r);
                        int.TryParse(txtFromG.Text, out g);
                        int.TryParse(txtFromB.Text, out b);
                        int x, y;
                        int.TryParse(txtToX.Text, out x);
                        int.TryParse(txtToY.Text, out y);
                        Color to = img.GetPixel(x, y);

                        for (x = 0; x < img.Width; x++)
                            for (y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == Color.FromArgb(255, r, g, b))
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromKnown.Checked)
                    {
                        int x, y;
                        int.TryParse(txtToX.Text, out x);
                        int.TryParse(txtToY.Text, out y);
                        Color to = img.GetPixel(x, y);

                        Color from;
                        try { from = Color.FromName(cmbFromKnown.Text); }
                        catch { return "Error"; }

                        for (x = 0; x < img.Width; x++)
                            for (y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == Color.FromArgb(255, from.R, from.G, from.B))
                                    img.SetPixel(x, y, to);
                    }
                }
                #endregion
                #region To RGB
                else if (rdoToRGB.Checked)
                {
                    if (rdoFromTransparent.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtToR.Text, out r);
                        int.TryParse(txtToG.Text, out g);
                        int.TryParse(txtToB.Text, out b);
                        Color to = Color.FromArgb(255, r, g, b);

                        for (var x = 0; x < img.Width; x++)
                            for (var y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y).A == 0)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromPixel.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtToR.Text, out r);
                        int.TryParse(txtToG.Text, out g);
                        int.TryParse(txtToB.Text, out b);
                        Color to = Color.FromArgb(255, r, g, b);

                        int x, y;
                        int.TryParse(txtFromX.Text, out x);
                        int.TryParse(txtFromY.Text, out y);
                        Color from = img.GetPixel(x, y);

                        for (x = 0; x < img.Width; x++)
                            for (y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == from)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromRGB.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtFromR.Text, out r);
                        int.TryParse(txtFromG.Text, out g);
                        int.TryParse(txtFromB.Text, out b);
                        Color from = Color.FromArgb(255, r, g, b);
                        int.TryParse(txtToR.Text, out r);
                        int.TryParse(txtToG.Text, out g);
                        int.TryParse(txtToB.Text, out b);
                        Color to = Color.FromArgb(255, r, g, b);

                        for (var x = 0; x < img.Width; x++)
                            for (var y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == from)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromKnown.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtToR.Text, out r);
                        int.TryParse(txtToG.Text, out g);
                        int.TryParse(txtToB.Text, out b);
                        Color to = Color.FromArgb(255, r, g, b);

                        Color from;
                        try { from = Color.FromName(cmbFromKnown.Text); }
                        catch { return "Error"; }

                        for (var x = 0; x < img.Width; x++)
                            for (var y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == Color.FromArgb(255, from.R, from.G, from.B))
                                    img.SetPixel(x, y, to);
                    }
                }
                #endregion
                #region To Known
                else if (rdoToKnown.Checked)
                {
                    Color to;
                    try { to = Color.FromName(cmbToKnown.Text); }
                    catch { return "Error"; }

                    if (rdoFromTransparent.Checked)
                    {
                        for (var x = 0; x < img.Width; x++)
                            for (var y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y).A == 0)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromPixel.Checked)
                    {
                        int x, y;
                        int.TryParse(txtFromX.Text, out x);
                        int.TryParse(txtFromY.Text, out y);
                        Color from = img.GetPixel(x, y);

                        for (x = 0; x < img.Width; x++)
                            for (y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == from)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromRGB.Checked)
                    {
                        int r, g, b;
                        int.TryParse(txtFromR.Text, out r);
                        int.TryParse(txtFromG.Text, out g);
                        int.TryParse(txtFromB.Text, out b);
                        Color from = Color.FromArgb(255, r, g, b);

                        for (var x = 0; x < img.Width; x++)
                            for (var y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == from)
                                    img.SetPixel(x, y, to);
                    }
                    else if (rdoFromKnown.Checked)
                    {
                        Color from;
                        try { from = Color.FromName(cmbFromKnown.Text); }
                        catch { return "Error"; }

                        for (var x = 0; x < img.Width; x++)
                            for (var y = 0; y < img.Height; y++)
                                if (img.GetPixel(x, y) == Color.FromArgb(255, from.R, from.G, from.B))
                                    img.SetPixel(x, y, to);
                    }
                }
                #endregion
            }
            catch
            {
                return "Error";
            }
            return "";
        }

        private string RemoveTransparent(ref Bitmap img)
        {
            try
            {
                for (var x = 0; x < img.Width; x++)
                    for (var y = 0; y < img.Height; y++)
                        if (img.GetPixel(x, y).A < 255)
                        {
                            Color from = img.GetPixel(x, y);
                            img.SetPixel(x, y, Color.FromArgb(255, from.R, from.G, from.B));
                        }
            }
            catch { return "Error"; }
            return "";
        }

        private string SaveFile(ref Bitmap img, FileInfo fi)
        {
            try
            {
                if (chkConvert.Checked)
                {
                    if (fi.Extension == ".png")
                        if (cmbExt.Text != ".png")
                            RemoveTransparent(ref img);
                    img.Save(txtSave.Text + "/" +
                        fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length) + cmbExt.Text);
                }
                else
                {
                    if (fi.Extension == ".png")
                        if (fi.Extension != ".png")
                            RemoveTransparent(ref img);
                    img.Save(txtSave.Text + "/" + fi.Name);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("There was an exception: " + e);
                MessageBox.Show("File caught exception at: " + fi.Name);
                return "Error";
            }
            return "";
        }
        #endregion

        private void btnFileConvert_Click(object sender, EventArgs e)
        {
            DeactivateControls();

            if (!File.Exists(txtFile.Text)) return;
            var f = new FileInfo(txtFile.Text);

            var img = new Bitmap(f.FullName);

            if (ModifyImage(ref img, f).Length > 1)
            {
                MessageBox.Show("Error occurred while modifying image...");
                ReactivateControls();
                return;
            }
            if (SaveFile(ref img, f).Length > 1)
            {
                MessageBox.Show("Error occurred while saving image...");
                ReactivateControls();
                return;
            }

            MessageBox.Show("Image converted.");
            ReactivateControls();
        }

        private void btnDirectoryConvert_Click(object sender, EventArgs e)
        {
            DeactivateControls();

            if (!Directory.Exists(txtDirectory.Text)) return;
            var cd = new DirectoryInfo(txtDirectory.Text);

            var i = 0;
            var errCount = 0;

            foreach (string c in chkExt.CheckedItems)
            {
                var cf = cd.GetFiles("*" + c);
                foreach (var f in cf)
                {
                    var img = new Bitmap(f.FullName);

                    if (ModifyImage(ref img, f).Length > 1 ||
                        SaveFile(ref img, f).Length > 1) errCount++;

                    i++;
                }
            }

            if (i > 0)
                MessageBox.Show("Completed: " + i + " image conversions with " + errCount + " Errors.");
            else
                MessageBox.Show("No image conversions were made.");

            ReactivateControls();
        }
    }
}
